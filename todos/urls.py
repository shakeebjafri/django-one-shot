from django.urls import path
from todos.views import todo_list_list, show_todos


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_todos, name="todos_object"),
]
