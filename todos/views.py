from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem
# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/list.html", context)


def show_todos(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todos_object": todos,
    }
    return render(request, "todos/detail.html", context)
